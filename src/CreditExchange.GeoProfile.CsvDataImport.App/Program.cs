﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Framework.Configuration;
using Microsoft.Framework.DependencyInjection;
using Microsoft.Framework.OptionsModel;
using System.Threading.Tasks;
using CreditExchange.GeoProfile.Persistence;
using LendFoundry.Foundation.Services;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Security.Tokens;
//using LendFoundry.Tenant.Client;
using Microsoft.AspNet.Http;
using Microsoft.AspNet.Http.Internal;

namespace CreditExchange.GeoProfile.CsvDataImport.App
{
    public class Program :DependencyInjection
    {
      
        public   void Main(string[] args)
        {
           
            StartImport().Wait();
            Console.ReadLine();
        }


        public static IConfigurationRoot Configuration { get; set; }

        

        public async  Task StartImport()
        {
            var loggerFactory = Provider.GetService<ILoggerFactory>();
            var logger = loggerFactory.Create(NullLogContext.Instance);
            try
            {
                logger.Debug("Resolving the services");
                var GeoProfileRepo = Provider.GetService<IGeoProfileRepository>();
                var csvParser = Provider.GetService<ICsvFileParser>();
                logger.Debug("Resolving of services completed");


                logger.Info($"Parsing of {Environment.GetEnvironmentVariable("CE_GeoPinCodeProfileCsvFilePath")} started");
                var geopincodeList = csvParser.ParseGeoPincodeCsvFile(Environment.GetEnvironmentVariable("CE_GeoPinCodeProfileCsvFilePath"));
                if (geopincodeList == null)
                {
                    logger.Info("Parsing of file failed. Skipping the process");
                    return;
                }
                var geopincode = geopincodeList as IList<GeoPincode> ?? geopincodeList.ToList();
                logger.Info($"Parsing of {Environment.GetEnvironmentVariable("CE_GeoPinCodeProfileCsvFilePath")} completed. Total {geopincode.Count} records parsed");

                logger.Info("Insert started for geo pincode");
               
                var result = await GeoProfileRepo.InsertGeoPincode(geopincode).ConfigureAwait(false);
                if (result)
                {
                    logger.Info($"Insert completed for geopincode. Total of {geopincode.Count} records processed ");
                }
            }
            catch (AggregateException aggregateException)
            {

                foreach (var aggregateExceptionInnerException in aggregateException.InnerExceptions)
                {
                    logger.Error(aggregateExceptionInnerException.Message, aggregateExceptionInnerException);
                }
            }
            catch (Exception exception)
            {
                logger.Error(exception.Message, exception);
            }
        }
        protected override IServiceCollection ConfigureServices(IServiceCollection services)
        {
            services.AddCors();
            services.AddCaching();
            services.AddMvc();
          
           
            //services.AddTenantTime();
            //services.AddTenantService("10.1.1.99", 5005);
            services.AddHttpServiceLogging("ConsoleAppForGeoProfile");
            services.AddTokenHandler();
            services.AddTransient<IHttpContextAccessor, HttpContextAccessor>();
           
            services.AddTransient<ICsvFileParser, CsvFileParser>();
          
            services.AddSingleton<IMongoConfiguration>(provider => GetMongoConfiguration());
            services.AddSingleton<IGeoProfileDbContext, GeoProfileDbContext>();
            services.AddSingleton<IGeoProfileRepository, GeoProfileRepository>();
            //services.AddTransient<IGeoProfileService, GeoProfileService>();

            services.Add(ServiceDescriptor.Transient(typeof(IOptions<>), typeof(OptionsManager<>)));
            services.AddServiceLogging("CSVGeoPincodeImport", NullLogContext.Instance);
            //var configurationBuilder = new ConfigurationBuilder().AddJsonFile("mongodbconfig.json");
            //Configuration = configurationBuilder.Build();
            //services.Configure<MongoDbSettings>(Configuration);
            return services;
        }

        private static IMongoConfiguration GetMongoConfiguration()
        {
           
           // var mongodbsettingsOptions = Provider.GetService<IOptions<MongoDbSettings>>();
           // var mongoDbSettings = mongodbsettingsOptions.Value;
            return new MongoConfiguration(Environment.GetEnvironmentVariable("CE_MongoConnectionString"), Environment.GetEnvironmentVariable("CE_MongoDatabase"));

        }
    }
}

