﻿#if DOTNET2
using  Microsoft.AspNetCore.Mvc;
#else
using Microsoft.AspNet.Mvc;
#endif
using LendFoundry.Foundation.Services;
using System;
using System.Threading.Tasks;
using LendFoundry.Foundation.Logging;

namespace CreditExchange.GeoProfile.Api
{
    [Route("/")]
    public class GeoProfileController : ExtendedController
    {
#region Declarations
        private ILoggerFactory LoggerFactory { get; }
       
        public IGeoProfileService GeoProfileService { get; }

#endregion

#region Constructor

        public GeoProfileController(IGeoProfileService geoProfileLookUpService, ILoggerFactory loggerFactory)
        {

            if (geoProfileLookUpService == null)
                throw new ArgumentNullException(nameof(geoProfileLookUpService));

            if (loggerFactory == null)
                throw new ArgumentNullException(nameof(loggerFactory));

            GeoProfileService = geoProfileLookUpService;
            LoggerFactory = loggerFactory;
           

        }

#endregion

        [HttpGet("{entityType}/{entityId}/pincode/verify/{pincode}")]
#if DOTNET2
        [ProducesResponseType(typeof(GeoPincodeResponse), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
#endif
        public async Task<IActionResult> GetPincode(string entityType, string entityId, string pincode)
        {
            GeoPincodeResponse objGeoPincodeResponse;
            try
            {
                if (string.IsNullOrEmpty(pincode))
                {
                    return ErrorResult.BadRequest("Pincode is required");
                }

                objGeoPincodeResponse = await GeoProfileService.GetPincode(entityType, entityId, pincode);

                return objGeoPincodeResponse.Zone == "Not Found" ? ErrorResult.NotFound($"Pincode {pincode} not found") : Execute(() => Ok(objGeoPincodeResponse));
            }           
            catch (Exception exception)
            {
                var logger = LoggerFactory.Create(NullLogContext.Instance);
                logger.Error(exception.Message, exception);
                return ErrorResult.InternalServerError(exception.Message);                
            }
        }
    }
}


