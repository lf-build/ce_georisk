﻿using LendFoundry.Security.Tokens;
#if DOTNET2
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.Framework.DependencyInjection;
#endif

namespace CreditExchange.GeoProfile.Client
{
    /// <summary>
    /// Geo Profile Service Client Extensions class
    /// </summary>
    public static class GeoProfileServiceClientExtensions
    {
        /// <summary>
        /// Adds the application service.
        /// </summary>
        /// <param name="services">The services.</param>
        /// <param name="endpoint">The endpoint.</param>
        /// <param name="port">The port.</param>
        /// <returns>Return service collection</returns>
        public static IServiceCollection AddGeoProfileService(this IServiceCollection services, string endpoint, int port)
        {
            services.AddTransient<IGeoProfileServiceClientFactory>(p => new GeoProfileServiceClientFactory(p, endpoint, port));
            services.AddTransient(p => p.GetService<IGeoProfileServiceClientFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }
    }
}
