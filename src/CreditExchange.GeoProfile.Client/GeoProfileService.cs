﻿using LendFoundry.Foundation.Client;
using RestSharp;
using System.Threading.Tasks;

namespace CreditExchange.GeoProfile.Client
{
    /// <summary>
    /// GeoProfile Service class
    /// </summary>
    /// <seealso cref="IGeoProfileService" />
    public class GeoProfileService : IGeoProfileService
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="GeoProfileService"/> class.
        /// </summary>
        /// <param name="client">The client.</param>
        public GeoProfileService(IServiceClient client)
        {
            Client = client;
        }

        /// <summary>
        /// Gets the client.
        /// </summary>
        /// <value>
        /// The client.
        /// </value>
        private IServiceClient Client { get; }

        /// <summary>
        /// Get /Verify the pincode
        /// </summary>
        /// <param name="entityId"></param>
        /// <param name="pincode">The Pincode </param>
        /// <param name="entityType"></param>
        /// <returns>GeoPincodeResponse object</returns>
        public async Task<GeoPincodeResponse> GetPincode(string entityType, string entityId, string pincode)
        {
            var request = new RestRequest("{entityType}/{entityId}/pincode/verify/{pincode}", Method.GET);
            request.AddUrlSegment("entityType", entityType);
            request.AddUrlSegment("entityId", entityId);
            request.AddUrlSegment("pincode", pincode);
            return await Client.ExecuteAsync<GeoPincodeResponse>(request);
        }


    }
}
