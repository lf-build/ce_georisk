﻿using LendFoundry.Security.Tokens;

namespace CreditExchange.GeoProfile.Client
{
    /// <summary>
    /// GeoProfile Service Client Factory interface
    /// </summary>
    public interface IGeoProfileServiceClientFactory
    {
        /// <summary>
        /// Creates the specified reader.
        /// </summary>
        /// <param name="reader">The reader.</param>
        /// <returns></returns>
        IGeoProfileService Create(ITokenReader reader);
    }
}
