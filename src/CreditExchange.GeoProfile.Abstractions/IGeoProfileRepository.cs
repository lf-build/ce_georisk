﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace CreditExchange.GeoProfile
{
    public interface IGeoProfileRepository
    {
        Task<bool> InsertGeoPincode(IEnumerable<GeoPincode> pincodeList);
        Task<GeoPincode> GetPincode(string pincode);

    }
}
