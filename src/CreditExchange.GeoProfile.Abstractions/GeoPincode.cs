﻿namespace CreditExchange.GeoProfile
{
    public class GeoPincode : IGeoPincode
    {
        public string Id { get; set; }


        public string Pincode
        {
            get;set;
        }

        
        public string City
        {
            get; set;
        }


        public string Zone
        {
            get; set;
        }

        
        public int GeoRiskRanking
        {
            get; set;
        }
        
    }
}
