﻿

namespace CreditExchange.GeoProfile
{
    public class GeoPincodeResponse
    {
        public string Pincode { get; set; }
        public string Zone { get; set; }
        public string City { get; set; }
        public int GeoRiskRanking { get; set; }
        public string ReferenceNumber { get; set; }
    }
}
