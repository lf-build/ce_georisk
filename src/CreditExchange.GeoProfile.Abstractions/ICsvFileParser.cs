﻿using System.Collections.Generic;

namespace CreditExchange.GeoProfile
{
    public interface ICsvFileParser
    {
        IEnumerable<GeoPincode> ParseGeoPincodeCsvFile(string filePath);

    }
}
