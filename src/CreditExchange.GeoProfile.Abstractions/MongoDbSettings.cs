﻿namespace CreditExchange.GeoProfile
{
    public class MongoDbSettings
    {
        public string Database { get; set; }
        public string MongoConnection { get; set; }
    }
}
