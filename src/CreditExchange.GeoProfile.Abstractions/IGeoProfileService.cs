﻿
using System.Threading.Tasks;

namespace CreditExchange.GeoProfile
{
    public interface IGeoProfileService
    {
        Task<GeoPincodeResponse> GetPincode(string entityType, string entityId,string pincode);
    }
}
