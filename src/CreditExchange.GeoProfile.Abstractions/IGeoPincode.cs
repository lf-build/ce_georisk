﻿namespace CreditExchange.GeoProfile
{
  public interface IGeoPincode
    {
        string Id { get; set; }
        string Pincode { get; set; }

        string City { get; set; }

        string Zone { get; set; } //This is Zone property 'Zone-1', 'Zone-2' etc.

        int GeoRiskRanking { get; set; } //Geo Pincode ranking property
        
    }
}
