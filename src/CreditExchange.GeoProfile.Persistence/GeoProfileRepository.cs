﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Driver;

namespace CreditExchange.GeoProfile.Persistence
{

    public class GeoProfileRepository : IGeoProfileRepository
    {
        private readonly IGeoProfileDbContext _geoProfileDbContext;

        public GeoProfileRepository(IGeoProfileDbContext geoProfileDbContext)
        {
            _geoProfileDbContext = geoProfileDbContext;
        }


        public async Task<bool> InsertGeoPincode(IEnumerable<GeoPincode> pincodeList)
        {
            await _geoProfileDbContext.GeoPinCodeRankingCollection.DeleteManyAsync(d => d.Pincode != string.Empty)
                    .ConfigureAwait(false);

            foreach (var geoPincode in pincodeList)
            {
                geoPincode.Id = ObjectId.GenerateNewId(DateTime.Now).ToString();
                await _geoProfileDbContext.GeoPinCodeRankingCollection.InsertOneAsync(geoPincode)
                  .ConfigureAwait(false);
            }

            return true;
        }

        public async Task<GeoPincode> GetPincode(string pincode)
        {

            var geoPinCode = await _geoProfileDbContext.GeoPinCodeRankingCollection.FindAsync(c => c.Pincode == pincode).ConfigureAwait(false);
                       
            return await geoPinCode.FirstOrDefaultAsync();
        }

    }
}




