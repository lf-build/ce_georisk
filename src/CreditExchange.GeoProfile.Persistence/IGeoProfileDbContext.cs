﻿using MongoDB.Driver;

namespace CreditExchange.GeoProfile.Persistence
{
    public interface IGeoProfileDbContext
    {
        IMongoDatabase Database { get; }

        IMongoCollection<GeoPincode> GeoPinCodeRankingCollection { get; }
    }
}