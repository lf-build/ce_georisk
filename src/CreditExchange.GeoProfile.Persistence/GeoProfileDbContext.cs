﻿using LendFoundry.Foundation.Persistence.Mongo;
using MongoDB.Driver;

namespace CreditExchange.GeoProfile.Persistence
{
    public class GeoProfileDbContext : IGeoProfileDbContext
    {
        public GeoProfileDbContext(IMongoConfiguration mongoConfiguration)
        {  
            var settings = MongoClientSettings.FromUrl(new MongoUrl(mongoConfiguration.ConnectionString));
         
            var client = new MongoClient(settings);
            Database = client.GetDatabase(mongoConfiguration.Database);
            GeoPinCodeRankingCollection = Database.GetCollection<GeoPincode>("geopincoderanking");
            
        }

        public IMongoDatabase Database { get; }
        public IMongoCollection<GeoPincode> GeoPinCodeRankingCollection { get; }
    }
}