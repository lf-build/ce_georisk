﻿using System.Threading.Tasks;
using System;
#if DOTNET2
using LendFoundry.EventHub;
#else
using LendFoundry.EventHub.Client;
#endif
using CreditExchange.GeoProfile.Events;
using LendFoundry.Foundation.Lookup;

namespace CreditExchange.GeoProfile
{
    public class GeoProfileService : IGeoProfileService
    {
        private IGeoProfileRepository GeoProfileRepository { get; }

        public GeoProfileService(IGeoProfileRepository geoProfileRepository, IEventHubClient eventHub, ILookupService lookup)
        {
            GeoProfileRepository = geoProfileRepository;
            EventHub = eventHub;
            Lookup = lookup;
        }
        private IEventHubClient EventHub { get; }
        private ILookupService Lookup { get; }
        private  static string ServiceName { get; } = "geoprofile";

        public async Task<GeoPincodeResponse> GetPincode(string entityType, string entityId,string pincode)
        {
            entityType = EnsureEntityType(entityType);

            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentException($"{nameof(entityId)} is mandatory");

            var searchResults = await GeoProfileRepository.GetPincode(pincode);
            GeoPincodeResponse objResponse = null;
            var referenceNumber = Guid.NewGuid().ToString("N");
            if (searchResults != null)
            {
                if (!string.IsNullOrWhiteSpace(searchResults.Zone))
                {
                    objResponse = new GeoPincodeResponse
                    {
                        Zone = searchResults.Zone,
                        Pincode = searchResults.Pincode,
                        City = searchResults.City,
                        GeoRiskRanking = searchResults.GeoRiskRanking,
                        ReferenceNumber = referenceNumber
                    };
                }
                else
                {
                    objResponse = new GeoPincodeResponse
                    {
                        Zone = "Others",
                        Pincode = pincode,
                        City = null,
                        ReferenceNumber = referenceNumber,
                        GeoRiskRanking = searchResults.GeoRiskRanking,
                    };
                }
               
             }
            else
            {
                objResponse = new GeoPincodeResponse
                {
                    Zone = "Not Found",
                    Pincode = pincode,
                    City =null,
                    ReferenceNumber = referenceNumber,
                    GeoRiskRanking = 0
                  
                };
            }
            await EventHub.Publish(new GeoRiskPinCodeVerified
            {
                EntityId = entityId,
                EntityType = entityType,
                Response = objResponse,
                Request = pincode,
                ReferenceNumber = referenceNumber,
                Name = ServiceName
            });
            return objResponse;
        }

        private string EnsureEntityType(string entityType)
        {
            if (string.IsNullOrWhiteSpace(entityType))
                throw new ArgumentException($"{nameof(entityType)} is mandatory");

            entityType = entityType.ToLower();

            if (Lookup.GetLookupEntry("entityTypes", entityType) == null)
                throw new ArgumentException($"{entityType} is not a valid entity");

            return entityType;
        }
    }
}