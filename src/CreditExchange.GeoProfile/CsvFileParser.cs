﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using LendFoundry.Foundation.Logging;

namespace CreditExchange.GeoProfile
{
    public class CsvFileParser : ICsvFileParser
    {
        private ILoggerFactory LoggerFactory { get; }

        public CsvFileParser(ILoggerFactory loggerFactory)
        {
            LoggerFactory = loggerFactory;
        }

        public IEnumerable<GeoPincode> ParseGeoPincodeCsvFile(string filePath)
        {
            var logger = LoggerFactory.Create(NullLogContext.Instance);

            var geoPincodeList = new List<GeoPincode>();
            logger.Debug($"Reading the GeoProfile CSV From Path : { filePath }");
            var lines = File.ReadAllLines(filePath);
            logger.Debug($"Reading the GeoProfile CSV file completed.");
            logger.Debug($"Skipping the CSV Header.");
            var csvData = lines.Skip(1).ToList(); // skip the header

            logger.Debug($"Reading csv record one by one.");

            foreach (var line in csvData)
            {
                logger.Debug($"Getting no of columns for csv record.");
                var columns = GetColumns(line);
                logger.Debug($"Checking the no of columns count for csv record.");
                if (!IsRequiredColumnsPresent(line))
                {
                    throw new InvalidOperationException($"Invalid columns count found. The expected columns should be 4 but actual are {columns.Length}. The line data is {@"\r\n"} {line}");
                }

                logger.Debug($"Assigning csv columns to GeoPincode model.");
                var geopincode = new GeoPincode
                {
                    Zone = columns[0],
                    City = columns[1],
                    Pincode = columns[2],
                    GeoRiskRanking = Convert.ToInt32(columns[3])
                };
                logger.Debug($"Assigning columns to GeoPincode model Completed.");
                logger.Debug($"Adding GeoPincode model to GeoPincodeList collection.");
                geoPincodeList.Add(geopincode);
            }

            logger.Debug($"Returing GeoPincodeList collection.");
            return geoPincodeList;
        }
        private static string[] GetColumns(string line)
        {
            var columns = line.Split(',');
            return columns;
        }
        private static bool IsRequiredColumnsPresent(string line)
        {
            var columns = line.Split(',');
            return columns.Length == 4;
        }
    }
}
