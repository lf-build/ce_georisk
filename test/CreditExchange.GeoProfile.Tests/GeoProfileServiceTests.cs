﻿using Moq;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Lookup;

namespace CreditExchange.GeoProfile.Tests
{
    public class CompanyServiceTests
    {
        //[Fact]
        //public void Given_Proper_GeoPincode_Then_Search_Should_Return_GeoPincode()
        //{
        //    // ARRANGE
        //    var mockRepo = new Mock<IGeoProfileRepository>();
        //    var eventHubObj = new Mock<IEventHubClient>();
        //    var LookupObj = new Mock<ILookupService>();
        //    var mockGeoPincodeList = new List<GeoPincode>
        //    {
        //        new GeoPincode
        //    {
        //        Id = "1",
        //        Pincode = "560001",
        //        City = "Banglore",
        //        GeoRiskRanking = 1,
        //        Zone = "zone-1"
        //    },
        //    new GeoPincode
        //    {
        //        Id = "2",
        //        Pincode = "560002",
        //        City = "Banglore",
        //        GeoRiskRanking = 2,
        //        Zone = "zone-2"
        //    }
        //};


                        
        //    mockRepo.Setup(x => x.GetPincode("560001")).Returns(Task.FromResult(mockGeoPincodeList[0]));
        //    mockRepo.Setup(x => x.GetPincode("560002")).Returns(Task.FromResult(mockGeoPincodeList[1]));
        //    var sut = new GeoProfileService(mockRepo.Object, eventHubObj.Object, LookupObj.Object);

        //    // ACT

        //    var searchResults = sut.GetPincode("entityType","entityId","560001").Result;

        //    //ASSERT

        //   // Assert.Equal(mockGeoPincodeList.Count(), 1);
        //    Assert.Equal(mockGeoPincodeList[0].Pincode, "560001");
        //    Assert.Equal(mockGeoPincodeList[0].Zone, "zone-1");
        //    Assert.Equal(mockGeoPincodeList[0].City, "Banglore");
        //    Assert.Equal(mockGeoPincodeList[0].GeoRiskRanking, 1);

        //}

        //[Fact]
        //public void Given_That_GeoPincode_Is_InValid_Then_It_Should_Give_A_Message()
        //{

        //    // ARRANGE
        //    var mockRepo = new Mock<IGeoProfileRepository>();
        //    var eventHubObj = new Mock<IEventHubClient>();
        //    var LookupObj = new Mock<ILookupService>();
        //    var mockGeoPincodeList = new List<GeoPincode>
        //    {
        //        new GeoPincode
        //    {
        //        Id = "1",
        //        Pincode = "560005",
        //        City = "Banglore",
        //        GeoRiskRanking = 1,
        //        Zone = "zone-1"
        //    },
        //    new GeoPincode
        //    {
        //        Id = "2",
        //        Pincode = "560006",
        //        City = "Banglore",
        //        GeoRiskRanking = 2,
        //        Zone = "zone-2"
        //    }
        //};


        //    // ARRANGE

        //    var geopincode = new GeoPincode
        //    {
        //        Pincode = "000000",
        //    };

        //    mockRepo.Setup(x => x.GetPincode("560005")).Returns(Task.FromResult(mockGeoPincodeList[0]));

        //    var sut = new GeoProfileService(mockRepo.Object, eventHubObj.Object, LookupObj.Object);

        //    // ACT

        //    var searchResults = sut.GetPincode("entityType","entityId",geopincode.Pincode).Result;

        //    // ASSERT

        //    Assert.Equal(searchResults, null);
        //}


    }
}
